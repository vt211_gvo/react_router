import { Routes, Route } from 'react-router-dom';
import Layout from './components/Layout';
import PrivateWrapper from './components/PrivateWrapper';
import Auth from './components/Auth';
import User from './components/User';
import './App.css';

function App() {
  return (
    <Layout>
      <Routes>
        <Route index element={<Auth />} />
        <Route element={<PrivateWrapper />}>
          <Route path='/user' element={<User />} />
        </Route>
      </Routes>
    </Layout>
  );
}

export default App;
