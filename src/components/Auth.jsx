import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Button, FormControl, TextField } from '@mui/material';
import { UserState } from '../contexts/UserProvider';

const Auth = () => {
  const navigate = useNavigate();
  const { setUser } = UserState();
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleSubmit = async () => {
    const response = await fetch('https://dummyjson.com/auth/login', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ username, password }),
    });
    const data = await response.json();

    setUser(data);
    navigate('/user');
  };

  return (
    <FormControl
      sx={{ m: 3, width: '40ch' }}
      component='fieldset'
      variant='standard'
      margin='dense'
    >
      <TextField
        label='Username'
        variant='standard'
        onChange={e => setUsername(e.target.value)}
      />
      <TextField
        label='Password'
        type='password'
        variant='standard'
        onChange={e => setPassword(e.target.value)}
      />
      <Button
        sx={{ m: 3 }}
        variant='contained'
        color='primary'
        onClick={handleSubmit}
      >
        Login
      </Button>
    </FormControl>
  );
};

export default Auth;
