import { UserState } from '../contexts/UserProvider';

const User = () => {
  const { user } = UserState();

  return (
    <div>
      <h1>User</h1>
      <p>{user.username}</p>
      <p>{user.email}</p>
    </div>
  );
};

export default User;
