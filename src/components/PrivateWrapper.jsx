import { Navigate, Outlet } from 'react-router-dom';
import { UserState } from '../contexts/UserProvider';

const PrivateWrapper = () => {
  const { user } = UserState();

  return user?.id ? <Outlet /> : <Navigate to='/' />;
};

export default PrivateWrapper;
