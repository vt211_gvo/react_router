import { Link, useNavigate } from 'react-router-dom';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import {
  BottomNavigation,
  BottomNavigationAction,
  Container,
  Paper,
} from '@mui/material';
import { UserState } from '../contexts/UserProvider';

const Layout = ({ children }) => {
  const navigate = useNavigate();
  const { setUser } = UserState();

  const logoutHandle = () => {
    setUser(undefined);
    navigate('/');
  };

  return (
    <>
      <Box sx={{ flexGrow: 1 }}>
        <AppBar position='static'>
          <Toolbar>
            <Typography variant='h6' component='div' sx={{ flexGrow: 1 }}>
              Auth App
            </Typography>
            <Link to='/'>
              <Button color='inherit' variant='contained'>
                Login
              </Button>
            </Link>
            <Link to='/user'>
              <Button color='inherit' variant='contained'>
                User
              </Button>
            </Link>
          </Toolbar>
        </AppBar>
      </Box>
      <Container align='center'>{children}</Container>
      <Paper
        sx={{
          position: 'fixed',
          bottom: 0,
          left: 0,
          right: 0,
        }}
      >
        <BottomNavigation showLabels>
          <BottomNavigationAction
            onClick={logoutHandle}
            label='Log out'
            color='secondary'
            classes={{
              fontSize: '2rem',
            }}
          />
        </BottomNavigation>
      </Paper>
    </>
  );
};

export default Layout;
